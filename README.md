# Knowledge Graphs in Vscope

## Resources

### Basic Introductions
* [What is Linked Data?](https://www.youtube.com/watch?v=4x_xzT5eF5Q)
* [What is JSON-LD?](https://www.youtube.com/watch?v=vioCbTo3C-4)
* [neo4j-onthologies](https://neo4j.com/blog/ontologies-in-neo4j-semantics-and-knowledge-graphs/)
* [Building an Enterprise Knowledge Graph at Uber: Lessons from Reality](https://www.youtube.com/watch?v=r3yMSl5NB_Q)

### Terms and definitions
* [Knowledge Graph](https://en.wikipedia.org/wiki/Knowledge_graph)
* [Knowledge Base](https://en.wikipedia.org/wiki/Knowledge_base)
* [Semantic network](https://en.wikipedia.org/wiki/Semantic_network)
* [Knowledge representation and reasoning](https://en.wikipedia.org/wiki/Knowledge_representation_and_reasoning)
* [Knowledge acquisition](https://en.wikipedia.org/wiki/Knowledge_acquisition)
* [Inference Engine](https://en.wikipedia.org/wiki/Inference_engine)
* [Symbolic artificial intelligence](https://en.wikipedia.org/wiki/Symbolic_artificial_intelligence)
* [Onthology](https://en.wikipedia.org/wiki/Ontology_(information_science))
* [Information management](https://en.wikipedia.org/wiki/Information_management)
* [Horn clause](https://en.wikipedia.org/wiki/Horn_clause#:~:text=A%20Horn%20clause%20is%20a%20clause%20(a%20disjunction%20of%20literals,called%20a%20dual%2DHorn%20clause.)
* [Formal specification](https://en.wikipedia.org/wiki/Formal_specification)
* [Algebraic specification](https://en.wikipedia.org/wiki/Algebraic_specification)
* [Formal methods](https://en.wikipedia.org/wiki/Formal_methods)
* [Logic programming](https://en.wikipedia.org/wiki/Logic_programming)
* [Inductive logic programming](https://en.wikipedia.org/wiki/Inductive_logic_programming)
* [Rule-base machine learning](https://en.wikipedia.org/wiki/Rule-based_machine_learning)
* [Non-monotonic logic](https://en.wikipedia.org/wiki/Rule-based_machine_learning)
* [Negation as failure](https://en.wikipedia.org/wiki/Negation_as_failure)
* [Deductive database](https://en.wikipedia.org/wiki/Deductive_database)
* [Datalog](https://en.wikipedia.org/wiki/Datalog)
* [Probabilistic logic networks](Probabilistic logic network)
* [ Bayesian networks ](https://en.wikipedia.org/wiki/Bayesian_network)
* [Probabilistic logic](https://en.wikipedia.org/wiki/Probabilistic_logic)

### Formats/Standards
* [Semantic Web](https://www.w3.org/standards/semanticweb/#:~:text=The%20term%20%E2%80%9CSemantic%20Web%E2%80%9D%20refers,SPARQL%2C%20OWL%2C%20and%20SKOS.)
* [OWL](https://www.w3.org/OWL/)
* [RDF](Organizations)
* [RDFa](http://rdfa.info/)
* [json-ld](https://json-ld.org/)
* [schema.org](https://schema.org/)
* [The Open Graph Protocol](https://ogp.me/)
* [Thrift](http://thrift.apache.org/docs/types)
* [avro](https://avro.apache.org/docs/current/spec.html)
* [protocol buffers](https://developers.google.com/protocol-buffers)
* [Shapes Constaints Language](https://www.w3.org/TR/shacl/)

### Playlist

* [Columbia SPS](https://www.youtube.com/playlist?list=PLAiy7NYe9U2Gjg-600CTV1HGypiF95d_D)

### Code
* [PorbLog](https://dtai.cs.kuleuven.be/problog/index.html)
* [Grakn](https://grakn.ai/)

### Events
* [Data-WS](https://www.w3.org/Data/events/data-ws-2019/)

### Examples:
 * [fibo](https://spec.edmcouncil.org/fibo/)
 * [Google Knowledge Graph](https://en.wikipedia.org/wiki/Knowledge_Graph)
 * [Thing](https://schema.org/Thing)
 * [yago](https://yago-knowledge.org/)
 * [dbpedia](https://wiki.dbpedia.org/)


